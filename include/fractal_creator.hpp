#ifndef FRACTAL_CREATOR_HPP
#define FRACTAL_CREATOR_HPP

#include <string>
#include <memory>
#include <vector>
#include <zoom_list.hpp>
#include <bitmap.hpp>
#include <rgb.hpp>

namespace fractal
{
    class fractal_creator
    {
        private:
            int width;
            int height;
            int total;
            zoom_list zl;
            std::unique_ptr<int[]> histogram;
            std::unique_ptr<int[]> iterations_per_pixel;
            bitmap bm;
            std::vector<int> ranges;
            std::vector<int> range_totals;
            std::vector<rgb> colors;

        private:
            void calculate_iterations ();
            void draw_fractal ();
            void add_zoom (const zoom& p_z);
            void write_bitmap (const std::string& p_filename);
            void calculate_totals ();
            int get_range (int iterations) const;
        
        public:
            fractal_creator(int p_width, int p_height);
            void add_range (double end_range, const rgb& p_rgb);
            void run (const std::string& p_filename);
    };
}

#endif /* fractal_creator.hpp */
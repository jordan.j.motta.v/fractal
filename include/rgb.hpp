#ifndef RGB_HPP
#define RGB_HPP

namespace fractal
{
    struct rgb
    {
        double r;
        double g;
        double b;
    };

    rgb operator- (const rgb&, const rgb&);
}

#endif /* rgb.hpp */
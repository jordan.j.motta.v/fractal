#ifndef MANDEBROT_HPP
#define MANDEBROT_HPP

namespace fractal
{
    class mandelbrot
    {
        public:
            static const int max_iterations = 10000;
            static int get_iterations (double x, double y);
    };
}
#endif /* mandebrot.hpp */
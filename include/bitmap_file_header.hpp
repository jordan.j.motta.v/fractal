#ifndef BITMAP_FILE_HEADER_HPP
#define BITMAP_FILE_HEADER_HPP

#include <cstdint>

namespace fractal
{
    #pragma pack(2)
    struct bitmap_file_header
    {
        char header[2]{'B', 'M'};
        std::int32_t file_size;
        std::int32_t reserved{0};
        std::int32_t data_offset;
    };
}

#endif /* bitmap_file_header.hpp */
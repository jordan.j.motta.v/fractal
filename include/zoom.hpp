#ifndef ZOOM_HPP
#define ZOOM_HPP

namespace fractal
{
    struct zoom
    {
        int x;
        int y;
        double scale;
        zoom (int p_x, int p_y, double p_scale) : x{p_x}, y{p_y}, scale{p_scale} {};
    };
}

#endif /* zoom.hpp */
#ifndef ZOOM_LIST_HPP
#define ZOOM_LIST_HPP

#include <vector>
#include <utility>
#include "zoom.hpp"

namespace fractal
{
    class zoom_list
    {
        private:
            double x_center{0.0};
            double y_center{0.0};
            double scale{1.0};
            int width;
            int height;
            std::vector<zoom>  zooms;
        public:
            zoom_list (int width, int height);
            void add (const zoom& z);
            std::pair<double, double> do_zoom (int x, int y);
    };
}

#endif /* zoom_list.hpp */
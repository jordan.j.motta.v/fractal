#ifndef BITMAP_HPP
#define BITMAP_HPP

#include <memory>
#include <string>
#include <cstdint>


#include "color.hpp"
namespace fractal
{

    class bitmap
    {
        private:
            std::size_t width;
            std::size_t height;
            std::unique_ptr<uint8_t[]> _pixels{nullptr};
        //
        public:
            bitmap (const std::size_t p_width,const std::size_t p_height);
            void set_pixel (const std::size_t x, const std::size_t y, const color& c);
            bool write (const std::string& filename);
    };

}

#endif /* bitmap.hpp */
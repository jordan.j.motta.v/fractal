#include <iostream>
#include <cstdint>
#include <memory>
#include <cmath>
#include <bitmap.hpp>
#include <mandelbrot.hpp>
#include <zoom_list.hpp>
#include <fractal_creator.hpp>

int main ()
{
    const int width = 800;
    const int height = 600;

    fractal::fractal_creator fc{width, height};
    fc.add_range (0.0, fractal::rgb{0, 255, 0});
    fc.add_range (0.3, fractal::rgb{255, 0, 255});
    fc.add_range (0.6, fractal::rgb{255, 255, 0});
    fc.add_range (1.0, fractal::rgb{255, 255, 255});
    fc.run ("test.bmp");
    std::cout << "Finished!\n";
    return 0;
}
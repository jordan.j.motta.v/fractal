#include <fstream>
#include <bitmap.hpp>
#include <bitmap_file_header.hpp>
#include <bitmap_info_header.hpp>

namespace fractal
{
    bitmap::bitmap (const std::size_t p_width,const std::size_t p_height) : 
    width{p_width}, height{p_height}, _pixels{std::make_unique<uint8_t[]>(p_width * p_height * 3)/*new uint8_t[p_width * p_height * 3]*/}
    {
        //
    }

    bool bitmap::write (const std::string& filename)
    {
        bitmap_file_header file_header;
        bitmap_info_header info_header;

        file_header.file_size = sizeof(bitmap_file_header) + sizeof (bitmap_info_header) + (width * height * 3);
        file_header.data_offset = sizeof(bitmap_file_header) + sizeof (bitmap_info_header);

        info_header.width = width;
        info_header.height = height;

        std::ofstream file;
        file.open (filename, std::ios::out | std::ios::binary);

        if (file.is_open ())
        {
            file.write (reinterpret_cast<char*>(&file_header), sizeof (file_header));
            file.write (reinterpret_cast<char*>(&info_header), sizeof (info_header));
            file.write (reinterpret_cast<char*>(_pixels.get ()), width * height * 3);
            file.close ();

            return true;
        }
        return false;
    }

    void bitmap::set_pixel (const std::size_t x, const std::size_t y, const color& c)
    {
        auto _pixel = _pixels.get();

        _pixel += (y * 3 * width) + (x * 3);

        //Little Endian
        _pixel[0] = c.b;
        _pixel[1] = c.g;
        _pixel[2] = c.r;
    }
}
#include <rgb.hpp>

namespace fractal
{
    rgb operator- (const rgb& l, const rgb& r)
    {
        return rgb{l.r - r.r, l.g - r.g, l.b - r.b};
    }
}
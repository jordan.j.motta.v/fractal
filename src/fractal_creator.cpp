#include <iostream>
#include <fractal_creator.hpp>
#include <mandelbrot.hpp>
#include <rgb.hpp>

namespace fractal
{
    fractal_creator::fractal_creator (int p_width, int p_height) : 
    width{p_width}, height{p_height}, zl{p_width, p_height}, bm{p_width, p_height},
    histogram{std::make_unique<int[]>(mandelbrot::max_iterations)},
    iterations_per_pixel{std::make_unique<int[]>(p_width*p_height)}
    {
        zl.add (fractal::zoom{width/2, height/2, 0.005});
    }

    void fractal_creator::add_range (double p_end_range, const rgb& p_rgb)
    {
        ranges.push_back (p_end_range * mandelbrot:: max_iterations);
        colors.push_back (p_rgb);

        if (ranges.size () > 1)
        {
            range_totals.push_back (0);
        }
    }

    void fractal_creator::calculate_totals ()
    {
        int range_index = 0;
        for (int i = 0; i < mandelbrot::max_iterations; ++i)
        {
            int pixels = histogram[i];
            if (i >= 0 && i <= ranges[range_index+1] )
            {
                range_totals[range_index] += pixels;
            }
            else
            {
                std::cout << "between 0 and " << range_index << "\n";
                ++range_index;
            }
        }

        for (int i = 0; i < range_totals.size (); ++i)
        {
            std::cout << range_totals[i] << " ";
        }
    }

    int fractal_creator::get_range (int iterations) const 
    {
        int range = 0;

        for (int i = 1; i < ranges.size (); ++i)
        {
            range = i;
            if (ranges[i] > iterations)
            {
                break;
            }
        }

        --range;

        return range;
    }

    void fractal_creator::run (const std::string& p_filename)
    {
        add_zoom (fractal::zoom{295, height - 202, 0.1});
        add_zoom (fractal::zoom{312, height - 304, 0.08});

        calculate_iterations ();
        calculate_totals ();
        draw_fractal ();
        write_bitmap ("test.bmp");
    }

    void fractal_creator::calculate_iterations ()
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                std::pair<double, double> coords = zl.do_zoom (x, y);
                int iterations = fractal::mandelbrot::get_iterations (coords.first, coords.second);
                iterations_per_pixel[(y * width) + x] = iterations;
                if (iterations != fractal::mandelbrot::max_iterations) histogram[iterations]++;
            }
        }

        total = 0;
        for (int i = 0; i < mandelbrot::max_iterations; ++i)
        {
            total += histogram[i];
        }
    }

    void fractal_creator::add_zoom (const zoom& z)
    {
        zl.add(z);
    }

    

    void fractal_creator::draw_fractal ()
    {
        color c;
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                c = color{0, 0, 0};
                int iterations = iterations_per_pixel[y * width + x];
                int range = get_range (iterations);
                int r_totals = range_totals[range];
                int range_start = ranges[range];
                fractal::rgb& start_color = colors[range];
                fractal::rgb& end_color = colors[range + 1];
                fractal::rgb diff_color = end_color - start_color;
                
                if (iterations != mandelbrot::max_iterations)
                {
                    int total_pixels = 0;
                    for (int i = range_start; i < iterations; ++i)
                    {
                        total_pixels += histogram[i];
                    }
                    c.r = start_color.r + diff_color.r * double(total_pixels)/r_totals;
                    c.g = start_color.g + diff_color.g * double(total_pixels)/r_totals;
                    c.b = start_color.b + diff_color.b * double(total_pixels)/r_totals;
                }

                bm.set_pixel (x, y, c);
                // if (component < min) min = component;
                // if (component > max) max = component;
            }
        }
    }

    void fractal_creator::write_bitmap (const std::string& p_filename)
    {
        bm.write (p_filename);
    }
}
#include <iostream>
#include <zoom_list.hpp>

namespace fractal
{
    zoom_list::zoom_list (int p_width, int p_height) : width{p_width}, height{p_height}
    {

    }
    void zoom_list::add (const zoom& z)
    {
        zooms.push_back (z);

        x_center += (z.x - width/2) * scale;
        y_center += (z.y - height/2) * scale;
        scale *= z.scale;

        // std::cout << x_center << ", " << y_center << ", " << scale << "\n";
    }

    std::pair<double, double> zoom_list::do_zoom (int x, int y)
    {
        double x_fractal = (x - width/2)*scale + x_center;
        double y_fractal = (y - height/2)*scale + y_center;
        // std::cout << x_fractal << ", " << y_fractal << "\n";
        return std::pair<double, double>(x_fractal, y_fractal);
    }
}
#include <complex>
#include <mandelbrot.hpp>

namespace fractal
{
    int mandelbrot::get_iterations (double x, double y)
    {
        std::complex<double> z = 0;
        std::complex<double> c(x, y);

        int iterations;

        for (iterations = 0; iterations < max_iterations; ++iterations)
        {
            z = z * z + c;

            if (std::abs(z) >= 2) break;
        }
        return iterations;

        
    }
}